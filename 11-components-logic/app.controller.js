(function () {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    myController.$inject = ['personService'];
    function myController(personService) {
        var vm = this;

        vm.persons = personService.get();

        vm.onChangeInput = function (value) {
            personService.add({ name: value, age: null, salary: 0 });
        };
    }

})();
