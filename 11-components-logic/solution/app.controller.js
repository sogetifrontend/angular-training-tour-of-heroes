(function() {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    myController.$inject = ['personService'];

    function myController(personService) {
        var vm = this;

        vm.persons = personService.get();

        vm.addPerson = function(name, age) {
            personService.add({ name: name, age: age, salary: 0 });
        };
    }

})();
