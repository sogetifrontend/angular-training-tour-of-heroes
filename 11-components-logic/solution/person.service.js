(function() {
    'use strict';

    angular
        .module('app')
        .service('personService', personService);

    function personService() {
        var persons = [
            { name: 'Alex', age: 35, salary: 3500 },
            { name: 'Abel', age: 29, salary: 3000 },
            { name: 'Cole', age: 54, salary: 3800 },
            { name: 'Hugo', age: 45, salary: 4000 },
            { name: 'John', age: 19, salary: 1800 },
            { name: 'Mike', age: 27, salary: 2700 }
        ];

        this.get = function() {
            return persons;
        };

        this.add = function(person) {
            persons.push(angular.copy(person));
            return persons;
        };
    }

})();
