(function() {
    'use strict';

    angular
        .module('app')
        .component('myInput', myInput());

    function myInput() {
        return {
            bindings: {
                nameLabel: '@',
                namePlaceholder: '@',
                ageLabel: '@',
                agePlaceholder: '@'
                // Add a callback by defining onClick as an ampersand (&)
            },
            templateUrl: './my-input.component.html'
        };
    }

}());
