'use strict';

var todoService;

beforeEach(setup);

describe('todoService', function() {
    it('should exist', function() {
        expect(todoService).to.exist;
    });

    describe('add', function() {
        it('should exist', function() {
            // Expect the add function to exist
        });

        it('should add a todo to the todos array', function() {
            // Create a mock todo object

            // Execute add function with the mocked todo

            // Expect the length of todoService.todos to be above 1
        });

        it('should return undefined in no todo is supplied', function() {
            // Create var result and make it equal to the result of the add function without parameters

            // Expect result to be undefined
        })
    });
});


function setup() {
    angular.mock.module('todo', function($provide) {
        // Mock TODO_STATUS with an empty object
        $provide.constant()
    });
    angular.mock.inject(function(TodoService) {
        // Assign TodoService to the global variable todoService
    })
}