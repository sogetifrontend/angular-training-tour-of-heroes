(function () {
    'use strict';

    angular
        .module('app')
        .service('personService', personService);

    function personService() {
        var persons = [
            { name: 'Albert Groothedde', age: 26 },
            { name: 'Jurgen Sweere', age: 35 },
            { name: 'Emiel Kwakkel', age: 27 }
        ];

        this.get = function () {
            return persons;
        };

        // create the function to add a person
    }

})();
