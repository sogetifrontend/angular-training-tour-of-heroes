(function() {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    myController.$inject = ['personService'];
    function myController(personService) {
        this.persons = personService.get();

        // create a function for the on-click event
    }
})();
