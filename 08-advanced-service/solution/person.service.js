(function() {
    'use strict';

    angular
        .module('app')
        .service('personService', personService);

    function personService() {
        var persons = [
            { name: 'Alex', age: 35 },
            { name: 'Abel', age: 29 },
            { name: 'Cole', age: 54 },
            { name: 'Hugo', age: 45 },
            { name: 'John', age: 15 },
            { name: 'Mike', age: 27 }
        ];

        this.get = function() {
            return persons;
        };

        this.add = function(person) {
            persons.push(angular.copy(person));
            return persons;
        };
    }

})();