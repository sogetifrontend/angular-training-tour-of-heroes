(function() {
    'use strict';

    angular
        .module('app')
        .component('personList', personList());


    function personList() {

        return {
            // create the binding for persons.
            bindings: {}
            // add the templateUrl to the Object.
        }
    }

}());