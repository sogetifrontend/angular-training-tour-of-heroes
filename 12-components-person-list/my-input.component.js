(function() {
    'use strict';

    angular
        .module('app')
        .component('myInput', myInput());

    function myInput() {
        return {
            bindings: {
                nameLabel: '@',
                namePlaceholder: '@',
                ageLabel: '@',
                agePlaceholder: '@',
                onClick: '&'
            },
            templateUrl: './my-input.component.html'
        };
    }

}());
