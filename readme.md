# AngularJs homework repository
This repository is built to support the AngularJSF course of Sogeti

# How to work
This is the `master` branch where you'll start the excercises. Each branch is a continuation of the previous one, so you will never have to be in trouble getting on par with the rest.

Every other branch has a `readme.md`. Here you'll find the instructions for that specific branch. If you look below you'll see the instructions for the master branch.

# Tasks `master`
1. Install nodejs and git on your machine
1. Clone this project using `git clone 'url'`
1. Open a terminal window and change directory to the project
1. Install the dependencies by running from terminal `npm install`
1. Start the application by running `npm start`

> Verify that you can now see the website on [http://localhost:3000/](http://http://localhost:3000/)
