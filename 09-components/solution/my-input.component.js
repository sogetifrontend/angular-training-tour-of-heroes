(function() {
    'use strict';

    angular
        .module('app')
        .component('myInput', myInput());

    function myInput() {
        return {
            templateUrl: './my-input.component.html'
        };
    }

}());
