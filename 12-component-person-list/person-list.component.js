(function () {
    'use strict';

    angular
        .module ('app')
        .component ('personList', personList());


    function personList() {

        return {
            // create the binding for persons
            bindings: {},
        }
    }

} ());