(function () {
    'use strict';

    angular
        .module('app')
        .service('personService', personService);

    function personService() {
        var persons = [
            { name: 'Albert Groothedde', age: 26, salary: 5 },
            { name: 'Jurgen Sweere', age: 35, salary: 5 },
            { name: 'Emiel Kwakkel', age: 27, salary: 5 }
        ];

        this.get = function () {
            return persons;
        };

        this.add = function (person) {
            persons.push(angular.copy(person));
            return persons;
        };
    }

})();
