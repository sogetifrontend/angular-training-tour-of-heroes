(function () {
    'use strict';

    angular
        .module ('app')
        .component ('personList', personList());


    function personList() {
        return {
            bindings: {
                persons: '<'
            },
            templateUrl: './person-list.component.html'
        }
    }

}());
