(function () {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    function myController() {
        this.persons = [
            { name: 'Albert Groothedde', age: 26 },
            { name: 'Jurgen Sweere', age: 35 },
            { name: 'Emiel Kwakkel', age: 27 }
        ];
    }
})();
