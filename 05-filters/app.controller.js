(function() {
    'use strict';

    // Look in index.html for the assignment.

    angular
        .module('app')
        .controller('myController', myController);

    function myController() {
        this.persons = [
            { name: 'Jurgen Sweere', age: 35 },
            { name: 'Emiel Kwakkel', age: 28 }
        ];
    }
})();
