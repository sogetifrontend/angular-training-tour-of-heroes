(function () {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    function myController() {
        this.persons = [
            { name: 'Jurgen Sweere', age: 35 },
            { name: 'Emiel Kwakkel', age: 28 }
        ];
    }
})();
