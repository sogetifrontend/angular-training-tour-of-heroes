(function() {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    myController.$inject = ['personService'];
    function myController(personService) {
        var vm = this;

        vm.persons = personService.get();
        vm.newPerson = {};

        vm.addPerson = function(newPerson) {
            vm.persons = personService.add(newPerson);
            vm.newPerson = {};
        };
    }

})();
