(function() {
    'use strict';

    angular
        .module('app')
        .filter('year', year);

    function year() {

        return yearFn;

        function yearFn(years, label) {
            return years + ' ' + label;
        }
    }

})();
