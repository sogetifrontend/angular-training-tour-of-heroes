(function() {
    'use strict';

    angular
        .module('app')
        .component('myInput', myInput());

    function myInput() {

        function myInputController() {
            var vm = this;

            vm.onButtonClick = function(localNgModel) {
                vm.onChange({ value: localNgModel });
            };
        }

        return {
            controller: myInputController,
            bindings: {
                label: '@',
                placeholder: '@',
                onChange: '&'
            },
            controllerAs: '$ctrl', // not mandatory if using $ctrl
            templateUrl: './my-input.component.html'
        };
    }

}());
