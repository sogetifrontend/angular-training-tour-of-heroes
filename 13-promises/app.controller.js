(function() {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    myController.$inject = ['githubService'];

    function myController(githubService) {
        var vm = this;

        vm.user = '';
        vm.loading = false;

        this.getUser = function(username) {
            // dont forget to set it to false after the request is finished
            vm.loading = true;
            // call the githubService.getByUsername

            // After the request is finished successfully: then()

            // After the request is finished unsuccessful: catch()

            // And what we want to do either way: finally()

        };
    }

})();
