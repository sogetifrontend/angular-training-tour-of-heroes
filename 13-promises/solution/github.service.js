(function() {
    'use strict';

    angular
        .module('app')
        .service('githubService', githubService);

    githubService.$inject = ['$http'];

    function githubService($http) {

        this.getByUsername = function(username) {
            return $http.get('https://api.github.com/users/' + username);
        };
    }

}());
