(function () {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    myController.$inject = ['githubService'];
    function myController(githubService) {
        var vm = this;

        vm.user = '';
        vm.loading = false;

        this.getUser = function (username) {
            vm.loading = true;
            githubService.getByUsername(username)
                // After the request is finished successfully
                .then(function (response) {
                    vm.user = response.data;
                })
                // After the request is finished unsuccessful
                .catch(function (response) {
                    vm.messages = response.data.message;
                })
                // And what we want to do either way
                .finally(function() {
                    vm.loading = false;
                });
        };
    }

})();
