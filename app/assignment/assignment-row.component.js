(function() {
    'use strict';

    angular
        .module('assignment')
        .component('assignmentRow', assignmentRow());

    function assignmentRow() {
        return {
            bindings: {
                url: '<',
                name: '<',
                description: '<',
                index: '<'
            },
            templateUrl: './app/assignment/assignment-row.html',
            controller: assignmentRowController
        }
    }

    assignmentRowController.$inject = ['$sce'];

    function assignmentRowController($sce) {
        var vm = this;

        vm.description = $sce.trustAsHtml(vm.description);
    }
})();
