(function () {
    'use strict';

    angular
        .module('assignment')
        .controller('assignmentController', assignmentController);

    assignmentController.$inject = ['AssignmentService'];

    function assignmentController(assignmentService) {
        this.assignments = assignmentService.getAssignments();
    }

})();
