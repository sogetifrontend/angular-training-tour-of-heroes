(function() {
    'use strict';

    angular
        .module('assignment')
        .service('AssignmentService', AssignmentService);

    function AssignmentService() {
        this.getAssignments = getAssignments;

        var assignments = [
            {
                'url': '01-expressions-binding',
                'name': 'Expressions and bindings',
                'description': 'Getting started with Angular using a expression to calculate and start working with one-way and two-way bindings.'
            },
            {
                'url': '02-module-controller',
                'name': 'Module and controllers',
                'description': 'Create a new module, request a module from angular and add a controller to the module'
            },
            {
                'url': '03-controlleras',
                'name': 'controllerAs',
                'description': 'Use controllerAs in the view to prevent name clashes.'
            },
            {
                'url': '04-directives',
                'name': 'Directives',
                'description': 'Angular has loads of directives free for you to use. We zoom in on ng-if and ng-repeat.'
            },
            {
                'url': '05-filters',
                'name': 'Filters',
                'description': 'Use an Angular filter to search a person array.'
            },
            {
                'url': '06-services',
                'name': 'Services',
                'description': 'Create a service and move the person array to the service. Add a function to get persons from the service.'
            },
            {
                'url': '07-advanced-service',
                'name': 'Advanced service',
                'description': 'Add a set function to the service.'
            },
            {
                'url': '08-custom-filter',
                'name': 'Custom filter',
                'description': 'We\'ll create a basic currency filter.'
            },
            {
                'url': '09-components',
                'name': 'Components',
                'description': 'Create a component to let the <code>my-input</code> html tag be replaced with a template.'
            },
            {
                'url': '10-components-bindings',
                'name': 'Component Bindings',
                'description': 'Add bindings to pass data from the view to the component.'
            },
            {
                'url': '11-components-logic',
                'name': 'Component Logic',
                'description': 'Add functions to the component.'
            },
            {
                'url': '12-components-person-list',
                'name': 'Component Person list',
                'description': 'Create a person list component.'
            },
            {
                'url': '13-promises',
                'name': 'Promises',
                'description': 'Use promises to load data async from an external data source.'
            },
            {
                'url': '14-routing',
                'name': 'Routing',
                'description': 'Use routing to add means of navigation between components.'
            }
        ];

        ////////////////

        function getAssignments() {
            return assignments;
        }
    }

})();
