(function() {
    angular.module('app')
        .config(function($stateProvider, $urlRouterProvider) {
            // For any unmatched url, redirect to home
            $urlRouterProvider.otherwise('/');

            // 2. Add a state for the contact page
            // 3. Create a contact component
            $stateProvider
                .state('home', {
                    url: '/',
                    template: '<home></home>'
                })
        });
})();
