(function() {
    'use strict';

    angular
        .module('app')
        .component('home', {
            templateUrl: 'home.component.html'
        });
}());
