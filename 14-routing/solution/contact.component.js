(function() {
    'use strict';

    angular
        .module('app')
        .component('contact', {
            templateUrl: 'contact.component.html'
        });
}());
