(function () {
  angular.module('app')
    .config(function ($stateProvider, $urlRouterProvider) {
      // For any unmatched url, redirect to home
      $urlRouterProvider.otherwise("/");

      // States
      $stateProvider
          .state('home', {
              url: '/',
              template: '<home></home>'
          })
          .state('contact', {
              url: '/contact',
              template: '<contact></contact>'
          });
    });
})();