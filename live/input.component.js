(function() {
    'use strict';
    var app = angular.module('app');

    app.component('myInput', myInput());

    function myInput() {
        function myController() {
            var vm = this;

            this.buttonClicked = function() {
                vm.onSave({
                    naam: vm.name,
                    age: vm.age
                });
                vm.name = '';
                vm.age = '';
            }
        }

        return {
            controller: myController,
            templateUrl: './my-input.component.html',
            bindings: {
                nameLabel: '@',
                ageLabel: '<',
                onSave: '&'
            },
            controllerAs: '$ctrl'
        }
    }
})();
