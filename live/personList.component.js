(function() {
    'use strict';
    var app = angular.module('app');

    app.component('personList', personListComponent());

    function personListComponent() {


        return {
            templateUrl: './personList.component.html',
            bindings: {
                persons: '<'
            },
            controllerAs: '$ctrl'
        }
    }
})();
