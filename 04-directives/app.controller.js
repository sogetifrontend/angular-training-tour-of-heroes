(function() {
    'use strict';

    // Look in index.html for the assignment.

    angular
        .module('app')
        .controller('myController', myController);

    function myController() {
        var vm = this;

        vm.persons = [
            { name: 'Jurgen Sweere', age: 35 },
            { name: 'Emiel Kwakkel', age: 28 }
        ];
    }

})();
