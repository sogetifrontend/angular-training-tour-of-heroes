(function() {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    function myController() {
        var vm = this;

        vm.persons = [
            { name: 'Jurgen Sweere', age: 35 },
            { name: 'Emiel Kwakkel', age: 28 }
        ];
    }

})();
