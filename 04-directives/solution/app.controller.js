(function () {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    function myController() {
        var vm = this;

        vm.persons = [
            { name: 'Albert Groothedde', age: 26 },
            { name: 'Jurgen Sweere', age: 35 },
            { name: 'Emiel Kwakkel', age: 27 }
        ];
    }

})();
