(function () {
    'use strict';

    angular
        .module('app')
        .component('myInput', myInput());

    function myInput() {
        return {
            // Add the bindings to the component
            bindings: {
            },
            controllerAs: '$ctrl', // not mandatory if using $ctrl
            templateUrl: './my-input.component.html'
        };
    }

}());
