(function () {
    'use strict';

    angular
        .module('app')
        .component('myInput', myInput());

    function myInput() {
        return {
            bindings: {
                label: '<',
                placeholder: '@'
            },
            controllerAs: '$ctrl', // not mandatory if using $ctrl
            templateUrl: './my-input.component.html'
        };
    }

}());