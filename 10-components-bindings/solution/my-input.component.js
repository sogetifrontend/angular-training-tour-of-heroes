(function() {
    'use strict';

    angular
        .module('app')
        .component('myInput', myInput());

    function myInput() {
        return {
            bindings: {
                label: '<', // < equals a one-way bound variable
                placeholder: '@' // @ equals string input
            },
            controllerAs: '$ctrl', // not mandatory if using $ctrl
            templateUrl: './my-input.component.html'
        };
    }

}());