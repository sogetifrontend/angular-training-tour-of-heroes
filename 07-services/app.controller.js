(function() {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    // Add $inject to myController
    // make it equal to an array containing personService as a string

    // Add personService as dependency to myController
    function myController() {
        var vm = this;

        // move the persons array to the personService
        // and retrieve it from the personService
        vm.persons = [
            { name: 'Albert Groothedde', age: 26 },
            { name: 'Jurgen Sweere', age: 35 },
            { name: 'Emiel Kwakkel', age: 27 }
        ];
    }
})();
