(function() {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    function myController() {
        var vm = this;

        vm.name = 'John';
        vm.nameInput = 'Mike';
        vm.updateName = function() {
            vm.name = vm.nameInput;
        }
    }

})();
