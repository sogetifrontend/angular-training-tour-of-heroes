(function () {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    function myController() {
        var vm = this;

        vm.name = 'Albert';
        vm.theInput = 'Jan';
        vm.buttonExpression = function () {
            vm.name = vm.theInput;
        }
    }

})();
