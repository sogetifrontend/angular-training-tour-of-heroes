(function () {
    'use strict';

    angular
        .module('app')
        .controller('myController', myController);

    // Replace '$scope' with 'this'
    // 'this' does not have to be injected
    function myController($scope) {
        // We're creating an alias for this, vm stands for ViewModel.
        var vm = this;

        // Replace '$scope' with 'vm'
        $scope.name = 'Albert';
        $scope.theInput = 'Jan';

        $scope.buttonExpression = function () {
            $scope.name = $scope.theInput;
        }
    }

})();
