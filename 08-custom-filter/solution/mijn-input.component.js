(function () {
    'use strict';

    angular
        .module('app')
        .component('mijnInput', {
            bindings: {
                label: '@'
            },
            transclude: true,
            controllerAs: '$ctrl',
            template:'{{$ctrl.label}} <input type="text"><ng-transclude></ng-transclude>'
        });    

} ());