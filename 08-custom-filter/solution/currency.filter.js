(function () {
    'use strict';

    angular
        .module('app')
        .filter('currency', currency);

    function currency() {

        return currencyFn;

        function currencyFn(amount) {
            return amount + ' euro';
        }
    }

})();
